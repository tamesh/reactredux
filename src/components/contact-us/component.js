import React, { Component } from 'react';

const ContactUsForm = (props) => {
    return (
        <div className='container'>
            <div className='row'>
                <div className='col-sm-10 col-sm-offset-1'>
                    <div className='well'>
                        <form>
                            <div className='row'>
                                <div className='col-sm-4'>
                                    <div className='form-group'>
                                        <label htmlFor='fname'>First Name</label>
                                        <input type='text' name='fname' className='form-control' />
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='lname'>Last Name</label>
                                        <input type='text' name='lname' className='form-control' />
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='email'>Email</label>
                                        <input type='text' name='email' className='form-control' />
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='subject'>Subject</label>
                                        <select name='subject' className='form-control'>
                                            <option>General Inquiry</option>
                                            <option>Site Suggestions</option>
                                            <option>Product Support</option>
                                        </select>
                                    </div>
                                </div>
                                <div className='col-sm-8'>
                                    <div className='form-group'>
                                        <label htmlFor='message'>Message</label>
                                        <textarea className='form-control' name='message' rows='10'></textarea>
                                    </div>
                                    <div className='text-right'>
                                        <input type='submit' className='btn btn-primary' value='Submit' />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default class LoginComponent extends Component{

    render() {
        return ( <ContactUsForm /> );
    }
}