import '../../assets/css/login.scss';
import React, { Component } from 'react';

import { connect } from 'react-redux';

import { saveData, getUserList, deleteUser } from './actions';
import LoginForm, { DisplayUser, DeleteButton} from './dumb-component';


const mapStateToProps = state => {
    return {
        userData: state.otherReducer.userData,
        userList: state.otherReducer.userList,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        actions: {
            saveData: (payload) => dispatch(saveData(payload)),
            getUserList: () => dispatch(getUserList()),
            deleteUser: (id) => dispatch(deleteUser(id))
        }
    }
};


class LoginComponent extends Component{

    constructor(props){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
    }

    componentDidMount() {
        this.props.actions.getUserList();
    }

    handleSubmit(values){
        this.props.actions.saveData(values);
    }

    deleteUser(item){
        this.props.actions.deleteUser(item);
    }

    render() {
        const userList = this.props.userList;
        return(
            <div>
                <LoginForm onSubmit={this.handleSubmit} />
                <DisplayUser userList={userList} deleteItem={this.deleteUser} />
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginComponent);