export const LOGIN = Object.freeze({
    LOGIN_PENDING: "LOGIN_PENDING",
    LOGIN_FULLFILL: "LOGIN_FULLFILL",
    LOGIN_ERROR: "LOGIN_ERROR",
    GET_LIST: "GET_LIST",
    DELETE_USER: "DELETE_USER"
});