import { Field, reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';

let LoginForm = props => {
    const { handleSubmit, pristine, submitting } = props;
        return (
            <div className="container">
                <div className="card card-container">
                    <img className="profile-img-card" src="../src/assets/images/avatar_2x.png" />
                    
                    <form className="form-signin" onSubmit={ handleSubmit }>
                        <span className="reauth-email"></span>
                        <Field name="userName" component="input" type="email" className="form-control" placeholder="Email address" />
                        <Field name="userPassword" component="input" type="password" className="form-control" placeholder="Password" />
                        <button className="btn btn-lg btn-primary btn-block btn-signin" type="submit" disabled={pristine || submitting}>Sign in</button>
                        <div className="row">
                            <div className="col">
                                <span className="checkbox">
                                    <label>
                                        <Field name="userRemember" component="input" type="checkbox" /> Remember me
                                    </label>
                                </span>
                            </div>
                            <div className="col">
                                <a href="#" className="forgot-password">Forgot the password?</a>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        )
};

LoginForm = reduxForm({ 
    form: 'loginForm', 
    enableReinitialize : true,
    initialValues: { userName: 'test@test.com', userPassword: '1234' }
})(LoginForm);
const selector = formValueSelector('loginForm');
export default LoginForm = connect(state => {
    const { userName, userPassword } = selector(state, 'userName', 'userPassword')
    return {
        userName: userName,
        userPassword: userPassword
    }
})(LoginForm);
  
export const DisplayUser = props => {
    const { userList, deleteItem } = props;
    return (
        <div className="col">
            <ul className="list-group">
                { 
                    userList.map((data, index) =>
                        <li className="list-group-item d-flex justify-content-between align-items-center" key={index}>
                            <span> { data.userName } / { data.userPassword } </span>
                            <span className="badge"> 
                                <DeleteButton deleteHandler={ deleteItem.bind(this, data) } />
                            </span>
                        </li>
                    )
                }
            </ul>
        </div>
    );
};

export const DeleteButton = (props) => {
    const { deleteHandler } = props;
    return (
        <button className="btn btn-danger btn-sm" onClick={deleteHandler}>Delete</button>
    );
}
