import React, { Component, PropTypes } from 'react';
import { Provider } from 'react-redux';
import LoginComponent from './component';
import store from './store';

export default class LoginWrapper extends Component {
    constructor(props) {
        super(props);
    }

    render(){
        return (
            <Provider store={store}>
        	    <LoginComponent  />
            </Provider>
        )
    }
}