import { combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';

import otherReducer from './login-reducer';
const loginFormReducer = { form: reduxFormReducer, otherReducer };

export default combineReducers(loginFormReducer);