import { LOGIN } from './constant';

const initialState = {
    userData: {},
    userList: []
};

export default function customContentReducer(state = initialState, action) {
    switch (action.type) {
        case LOGIN.LOGIN_FULLFILL:
            return Object.assign({}, state, {userList: state.userList.concat(action.data) })
            break;

        case LOGIN.GET_LIST:
            return Object.assign({}, state, {userList: action.data});
            break;

        case LOGIN.DELETE_USER:
            return Object.assign({}, state, {userList: state.userList.filter(item => action.data !== item) });
            break;

        default:
            return state;
    }
}