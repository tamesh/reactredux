import axios from 'axios';
import { LOGIN } from './constant';

const guid = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

export const saveData = (payload) => {
    return dispatch => {
        
        axios.post('http://localhost:3000/userLogin', payload)
            .then(response => {
                dispatch({
                    type: LOGIN.LOGIN_FULLFILL,
                    data: Object.assign({}, payload, {id: guid()})
                });
            })
            .catch( (error) => dispatch({ type: LOGIN.LOGIN_ERROR }) )
    }
};

export const getUserList = () => {
    return dispatch => {
        
        axios.get('http://localhost:3000/userLogin')
            .then(response => {
                dispatch({
                    type: LOGIN.GET_LIST,
                    data: response.data
                });
            })
            .catch( (error) => dispatch({ type: LOGIN.LOGIN_ERROR }) )
    }
};

export const deleteUser = (item) => {
    return dispatch => {
        
        axios.delete(`http://localhost:3000/userLogin/${item.id}`)
            .then(response => {
                dispatch({
                    type: LOGIN.DELETE_USER,
                    data: item
                });
            })
            .catch( (error) => dispatch({ type: LOGIN.LOGIN_ERROR }) )
    }
};