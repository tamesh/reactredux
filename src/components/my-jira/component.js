import '../../assets/css/my-jira.scss';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactModal from 'react-modal';
import {reset} from 'redux-form';
ReactModal.setAppElement('#root');
import { 
    saveData, 
    getUserList, 
    deleteItemList, 
    editItemList, 
    changeItemStatus, 
    modalClose, 
    updateItem, 
    deleteItemApi, confirmModal } from './actions';
import JiraForm, { DisplayList, OkCancelPopup } from './dumb-component';


const mapStateToProps = state => {
    return {
        item: state.jiraReducer.item,
        items: state.jiraReducer.items,
        showModal: state.jiraReducer.showModal,
        deleteModal: state.jiraReducer.deleteModal,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        actions: {
            saveData: (item) => dispatch(saveData(item)),
            getUserList: () => dispatch(getUserList()),
            deleteItemList: (item) => dispatch(deleteItemList(item)),
            editItemList: (item) => dispatch(editItemList(item)),
            changeItemStatus: (item) => dispatch(changeItemStatus(item)),
            modalClose: (bool) => dispatch(modalClose(bool)),
            updateItem: (item) => dispatch(updateItem(item)),
            resetForm: () => dispatch(reset('jiraForm')),
            deleteItemApi: (item) => dispatch(deleteItemApi(item)),
            confirmModal: (bool) => dispatch(confirmModal(bool)),
        }
    }
};


class JiraComponent extends Component{

    constructor(props){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.editItem = this.editItem.bind(this);
        this.changeStatus = this.changeStatus.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
        this.resetForm = this.resetForm.bind(this);
        this.closeConifrmModal = this.closeConifrmModal.bind(this);
    }

    componentDidMount() {
        this.props.actions.getUserList();
    }

    handleSubmit(values){
        this.props.actions.saveData(values);
    }

    deleteItem(item){
        this.props.actions.deleteItemList(item);
    }

    editItem(item){
        this.props.actions.editItemList(item);
    }

    changeStatus(item) {
        this.props.actions.changeItemStatus(item);
    }

    resetForm() {
        this.props.actions.resetForm();
    }

    handleCloseModal() {
        this.props.actions.modalClose(false);
    }

    handleUpdate(item) {
        this.props.actions.updateItem(item);
    }

    deleteSelectedItem(item) {
        this.props.actions.deleteItemApi(item);
    }

    closeConifrmModal() {
        this.props.actions.confirmModal(false);
    }

    render() {
        
        const {items, showModal, deleteModal, item} = this.props;
        return(
            <div className="container">
                <JiraForm onSubmit={this.handleSubmit} handleCancel={this.resetForm} actionLabel="Save" resetLabel="Reset" />
                <DisplayList items={items}  deleteItem={this.deleteItem} editItem={this.editItem} statusAction={this.changeStatus} />

                <ReactModal isOpen={showModal} className="modal-container" overlayClassName="modal-overlay">
                    <div className="modalBody">
                        <JiraForm onSubmit={this.handleUpdate} handleCancel={this.handleCloseModal} actionLabel="Update" resetLabel="Cancel" />
                    </div>
                </ReactModal>

                <ReactModal isOpen={deleteModal} className="modal-container" overlayClassName="modal-overlay" portalClassName="PopUp-Portal">
                    <div className="modalBody">
                        <OkCancelPopup okHandler={this.deleteSelectedItem.bind(this, item)} cancelHandler={this.closeConifrmModal} />
                    </div>
                </ReactModal>
            </div>
        );
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(JiraComponent);