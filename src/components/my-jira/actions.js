import axios from 'axios';
import {reset} from 'redux-form';
import { JIRA } from './constant';
import { guid } from '../../common/utiliti-helper';

export const saveData = (payload) => {
    return dispatch => {
        const formatedPayload = Object.assign({}, payload, {id: guid()});
        axios.post('http://localhost:3000/jiraList', formatedPayload)
            .then(response => {
                dispatch({
                    type: JIRA.JIRA_FULLFILL,
                    data: formatedPayload
                });
                dispatch(reset('jiraForm'));
            })
            .catch( (error) => dispatch({ type: JIRA.JIRA_ERROR }) )
    }
};

export const getUserList = () => {
    return dispatch => {
        
        axios.get('http://localhost:3000/jiraList')
            .then(response => {
                dispatch({
                    type: JIRA.GET_LIST,
                    data: response.data
                });
            })
            .catch( (error) => dispatch({ type: JIRA.JIRA_ERROR }) )
    }
};

export const deleteItemList = (item) => {
    return dispatch => {
        dispatch({
            type: JIRA.OPEN_MODAL,
            data: item,
            deleteModal: true
        });
    }
};

export const deleteItemApi = (item) => {
    return dispatch => {
        axios.delete(`http://localhost:3000/jiraList/${item.id}`)
            .then(response => {
                dispatch({
                    type: JIRA.DELETE_USER,
                    data: item,
                    deleteModal: false
                });
            })
            .catch( (error) => dispatch({ type: JIRA.JIRA_ERROR }) )
    }
}

export const editItemList = (item) => {
    return dispatch => {
        dispatch({
            type: JIRA.EDIT_LIST,
            data: item,
            showModal: true
        });
    }
}

export const changeItemStatus = (payload) => {
    payload.status = JIRA.STATUS_DONE;
    const formatedPayload = Object.assign({}, payload);
    return dispatch => {

        axios.put(`http://localhost:3000/jiraList/${formatedPayload.id}`, formatedPayload)
            .then(response => {
                dispatch({
                    type: JIRA.ITEM_CHANGE_STATUS,
                    data: formatedPayload,
                    id: formatedPayload.id
                });
            })
            .catch( (error) => dispatch({ type: JIRA.JIRA_ERROR }) )
    }
}

export const modalClose = (bool) => {
    return dispatch => {
        dispatch({
            type: JIRA.CLOSE_MODAL,
            showModal: bool
        });
    }
}

export const updateItem = (item) => {
    return dispatch => {
        axios.put(`http://localhost:3000/jiraList/${item.id}`, item)
            .then(response => {
                dispatch({
                    type: JIRA.UPDATE_LIST,
                    data: item,
                    showModal: false
                });
                dispatch(reset('jiraForm'));
            })
            .catch( (error) => dispatch({ type: JIRA.JIRA_ERROR }) )
    }
}

export const confirmModal = (bool) => {
    return dispatch => {
        dispatch({
            type: JIRA.CLOSE_CONFIRM_MODAL,
            deleteModal: bool,
        });
    }
}