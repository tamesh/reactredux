import { Field, reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

let JiraForm = ({ handleSubmit, pristine, submitting, handleCancel, actionLabel, resetLabel }) => {
        return (
            <div className="col">
                <div className="card">
                    <div className="card-header bg-primary text-white">
                        <b>Add Jira Task</b>
                    </div>
                    <div className="card-body">
                        <form onSubmit={ handleSubmit }>
                            <div className="form-row d-flex flex-row">
                                <div className="form-group flex-grow-1 mr-4">
                                    <div>
                                        <label>ID</label>
                                        <Field name="jiraId" component="input" type="text" className="form-control" placeholder="Jira ID, ex: BPIEOL-2012" />
                                    </div>
                                    <div>
                                        <label>Status</label>
                                        <Field name="status" component="select" className="form-control" placeholder="Status">
                                            <option value="PENDING" defaultValue="PENDING">PENDING</option>
                                            <option value="DONE">DONE</option>
                                            <option value="COMPLETE">COMPLETE</option>
                                        </Field>
                                    </div>
                                </div>
                                <div className="form-group flex-column flex-fill">
                                    <div className="">
                                        <label>Description</label>
                                        <div className="d-flex flex-fill">
                                            <Field name="jiraDescription" component="textarea" type="text" className="form-control" placeholder="Short Description" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div className="container-fluid">
                                    <div className="row">
                                        <button className="btn btn-lg btn-primary btn-sm mr-2 px-4" type="submit" disabled={pristine || submitting}>{ actionLabel }</button>
                                        <button className="btn btn-lg btn-danger btn-sm px-2" type="button" disabled={submitting} onClick={handleCancel}> { resetLabel }</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
};

JiraForm = reduxForm({ 
    form: 'jiraForm', 
    enableReinitialize : true
})(JiraForm);

JiraForm = connect( state => ({
    initialValues: Object.assign({}, state.jiraReducer.item, {status: "PENDING"})
}),
)(JiraForm)
export default JiraForm

  
export const DisplayList = props => {
    const { items, deleteItem, editItem, statusAction } = props;
    if(items.length){
        return (
            <div className="col mt-3">
                <ul className="list-group">
                    <li className="list-group-item bg-primary text-white flex-wrap d-flex justify-content-between align-items-center">
                        <div className="col-8">
                            <div className="row">
                                <b>Jira Task - Description</b>
                            </div>
                        </div>
                        <div className="col-2">
                            <b>Status</b>
                        </div>
                        <div className="col-2">
                            <b>Action</b>
                        </div>
                    </li>
                    { 
                        items.map( (data, index) => {
                            return (
                                <li className="reset-padding flex-wrap d-flex justify-content-between" key={index}>
                                    <div className="col-8">{ data.jiraId } : { data.jiraDescription }</div>
                                    <div className="col-2">
                                        <div className="row">
                                            { data.status }
                                        </div>
                                    </div>
                                    <div className="col-2">
                                        <div className="row">
                                            <EditItem editHandler={ editItem.bind(this, data) } />
                                            <DeleteButton deleteHandler={ deleteItem.bind(this, data) } />
                                        </div>
                                    </div>
                                </li>
                            )}
                        )
                    }
                </ul>
            </div>
        );
    }
    else{
        return(
            <NoRecordsFound />
        );
    }
};

DisplayList.propTypes = {
    items: PropTypes.array.isRequired,
    deleteItem: PropTypes.func.isRequired,
    editItem: PropTypes.func.isRequired,
    statusAction: PropTypes.func.isRequired,
};

export const DeleteButton = (props) => {
    const { deleteHandler } = props;
    return (
        <button type="button" className="btn fa fa-trash text-danger" onClick={deleteHandler}></button>
    );
}

DeleteButton.propTypes = {
    deleteHandler: PropTypes.func.isRequired
};

export const EditItem = (props) => {
    const { editHandler } = props;
    return (
        <button type="button" className="btn fa fa-edit text-success" onClick={editHandler}></button>
    );
}

EditItem.propTypes = {
    editHandler: PropTypes.func.isRequired
}


export const NoRecordsFound = (props) => {
    return (
        <div className="col mt-3">
            <div className="alert alert-danger text-danger">
                <em className="fa fa-exclamation-circle"></em> Task have not been added.
            </div>
        </div>
    );
}

export const StatusComponent = (props) => {
    const {label, statusHandler} = props;
    return (
        <button type="button" className="btn btn-success btn-sm" onClick={statusHandler}>{label}</button>
    )
}

StatusComponent.propTypes = {
    label: PropTypes.string.isRequired,
    statusHandler: PropTypes.func.isRequired,    
}

export const OkCancelPopup = (props) => {
    const {okHandler, cancelHandler} = props;

    return (
        <div className="alert-popup  card">
            <div className="card-body">
                <div className="alert-popup_content card-title">
                    <h5 className="card-title">Are you sure want to delete jira task?</h5>
                </div>
                <div className="alert-popup_action mt-4">
                    <button type="button" className="btn btn-primary btn-sm mr-2 px-4" onClick={okHandler}>Ok</button>
                    <button type="button" className="btn btn-danger btn-sm" onClick={cancelHandler}>Cancel</button>
                </div>
            </div>
        </div>
    )
}
OkCancelPopup.propTypes = {
    okHandler: PropTypes.func.isRequired,
    cancelHandler: PropTypes.func.isRequired,
}

