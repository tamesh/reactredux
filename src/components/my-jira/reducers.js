import { combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';

import jiraReducer from './my-jira-reducer';
const jiraFormReducer = { form: reduxFormReducer, jiraReducer };

export default combineReducers(jiraFormReducer);