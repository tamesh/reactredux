import React, { Component } from 'react';
import { Provider } from 'react-redux';
import JiraComponent from './component';
import store from './store';
import '../../assets/css/my-jira.scss';

export default class MyJira extends Component {
    constructor(props) {
        super(props);
    }

    render(){
        return (
            <Provider store={store}>
        	    <JiraComponent  />
            </Provider>
        )
    }
}