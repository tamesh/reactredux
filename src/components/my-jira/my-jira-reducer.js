import { JIRA } from './constant';
import {updateObjectInArray} from '../../common/utiliti-helper';
const initialState = {
    item: {},
    items: [],
    showModal: false,
    deleteModal: false
};

export default function customContentReducer(state = initialState, action) {
    switch (action.type) {
        case JIRA.JIRA_FULLFILL:
            return Object.assign({}, state, {items: state.items.concat(action.data) });
            break;

        case JIRA.GET_LIST:
            return Object.assign({}, state, {items: action.data});
            break;

        case JIRA.DELETE_USER:
            return Object.assign({}, state, {item:null, deleteModal: action.deleteModal, items: state.items.filter(item => action.data !== item) });
            break;
        
        case JIRA.EDIT_LIST:
            return Object.assign({}, state, {showModal: action.showModal, item: action.data});
            break;
        
        case JIRA.CLOSE_MODAL:
            return Object.assign({}, state, {showModal: action.showModal});
            break;

        case JIRA.OPEN_MODAL:
            return Object.assign({}, state, {item: action.data, deleteModal: action.deleteModal});
            break;
        
        case JIRA.CLOSE_CONFIRM_MODAL:
            return Object.assign({}, state, {item: null, deleteModal: action.deleteModal});
            break;

        case JIRA.ITEM_CHANGE_STATUS:
            return Object.assign({}, state, {item: action.data });
            break;
        
        case JIRA.UPDATE_LIST:
            return Object.assign({}, state, {
                items: updateObjectInArray(state.items, action),
                showModal: action.showModal
            });
            break;

        default:
            return state;
    }
}