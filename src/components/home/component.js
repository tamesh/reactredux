import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Modal from 'react-modal';

import { getList, openImageModal, closeImageModal, deleteImage } from './actions';
import OpenImage from './OpenImage';


const mapStateToProps = state => {
	return {
		homeReducer : state.homeReducer
	};
}

const mapDispatchToProps = dispatch => {
	return {
		actions : {
			getList: () => dispatch(getList()),
			openImageModal: (imagepath) => dispatch(openImageModal(imagepath)),
            closeImageModal: () => dispatch(closeImageModal()),
            deleteImage : (item) => dispatch(deleteImage(item)),
		}
	};
}


class HomeComponent extends Component{

	constructor(props){
		super(props);
		// this.handleOpenClick = this.handleOpenClick.bind(this);
		this.deleteImage = this.deleteImage.bind(this);
		this.printImage = this.printImage.bind(this);
		this.openImageModal = this.openImageModal.bind(this);
	}

	componentDidMount(){
		this.props.actions.getList();
	}

	openImageModal(imagePath){
		this.props.actions.openImageModal(imagePath);
		return <OpenImage 
						showImage={imagePath}
						onImageCloseClick={this.props.actions.closeImageModal.bind(this)}
					/>
	}

	deleteImage(item){
		let data = this.props.homeReducer.contentData;
		data.splice(data.indexOf(item), 1);
		this.props.actions.deleteImage(item);
	}

	printImage(e){
		
	}

	render(){
		const ImageCard = (props) => {
			const fieldValue = props.itemList;
			return (
				<div className="col-md">
						<div className="imgClass">
							
							<img src={fieldValue.imageLocation} width="100%"/>
							<div className="overlay">
								<a className="btn btn-primary btn-sm" href="javascript:void(0);" onClick={ () => this.openImageModal(fieldValue.imageLocation) }>Open</a>
								<a className="btn btn-primary btn-sm" href="javascript:void(0);" onClick={ () => this.deleteImage(fieldValue)}>Delete</a>
							</div>
						</div>
						<div className="imageTextStyle">
							{ fieldValue.imageText }
						</div>
					</div>
			)
		}
		
		return(
			<div className="container">
				<div className="row">
					{
						this.props.homeReducer.contentData.map((fieldValue) =>
							<ImageCard key={fieldValue.id} itemList={fieldValue} />
						)
					}
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeComponent);