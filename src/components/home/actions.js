import axios from 'axios';

export const getList = () => {
    return dispatch => {
        dispatch({ type: 'IMAGE_DATA_PENDING' });
        
        axios.get('http://localhost:3000/imgData')
            .then(response => {
                dispatch({
                    type: 'IMAGE_DATA_FULLFILL',
                    data: response.data
                });
            })
            .catch( () => dispatch({ type: 'IMAGE_DATA_ERROR' }) )
    }
};

export const openImageModal = (imgpath) => {
    return {
        type: 'OPEN_IMAGE_MODAL',
        clickedImage: imgpath,
        imageModalVisibility: true
    }
};

export const closeImageModal = () => {
    return {
        type: 'CLOSE_IMAGE_MODAL',
        imageModalVisibility: false
    }
};

export const deleteImage = (item) => {
    return {
        type: 'DELETE_IMAGE',
        deleteElementId: item
    }
};