import React, { Component, PropTypes } from 'react';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import logger from 'redux-logger'
import '../../assets/css/Home.scss';

import HomeComponent from './component';
import rootReducer from './reducers';

const store = createStore(rootReducer, compose(
    applyMiddleware(thunk, logger),
    window.devToolsExtension ? window.devToolsExtension() : f => f
));

export default class Home extends Component {
    constructor(props) {
        super(props);
    }
    render(){
        return (
            <Provider store={store}>
        	    <HomeComponent />   
            </Provider>
        )
    }
}