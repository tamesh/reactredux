import React, { Component } from 'react';
import Modal from 'react-modal';
Modal.setAppElement('#root');

export default class OpenImage extends Component{
	constructor(props){
		super(props);
	}

	render() {
		return(
			<span>
				<Modal isOpen={this.props.imageModalVisibility} contentLabel="Modal Window" className="ModalClass" overlayClassName="OverlayClass" >
					<div className='centerContainer'>
						<div className='mainContainer'>
							<img src={this.props.imgpath} height="200px"/>

							<div className="topPadding">
								<button className="btn btn-danger btn-sm" onClick={this.props.onImageCloseClick}>Close</button>
							</div>
						</div>
					</div>
				</Modal>
			</span>
		);
	}
}