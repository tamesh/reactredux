const initialState = {
    contentData : [],
    imageModalVisibility : null,
    clickedImage: null,
    deleteImage : null
};

const ACTION_LIST = Object.freeze({
    IMAGE_DATA_PENDING: "IMAGE_DATA_PENDING",
    IMAGE_DATA_FULLFILL: "IMAGE_DATA_FULLFILL",
    IMAGE_DATA_ERROR: "IMAGE_DATA_ERROR",
    OPEN_IMAGE_MODAL: "OPEN_IMAGE_MODAL",
    CLOSE_IMAGE_MODAL: "CLOSE_IMAGE_MODAL",
    DELETE_IMAGE: "DELETE_IMAGE"
})

export default function customContentReducer(state = initialState, action) {
    switch (action.type) {

        case ACTION_LIST.IMAGE_DATA_FULLFILL :
            return Object.assign({}, state, { contentData: action.data });
        break;

        case ACTION_LIST.OPEN_IMAGE_MODAL :
            return Object.assign({}, state, { imageModalVisibility : action.imageModalVisibility, clickedImage : action.clickedImage });
        break;

        case ACTION_LIST.CLOSE_IMAGE_MODAL :
            return Object.assign({}, state, { imageModalVisibility : action.imageModalVisibility });
        break;

        case ACTION_LIST.DELETE_IMAGE :
            return Object.assign({}, state, { deleteImage : action.deleteElementId });
        break;
        
        default:
            return state;
    }
}