import ABOUT_US from './constant';
const initialState = {
    employeeList : [],
    ft: [],
    pt: [],
    pr: [],
};

export default function aboutUsReducers(state = initialState, action) {
    switch (action.type) {

        case ABOUT_US.EMPLOYEE_LIST:
            return Object.assign({}, state, {
                employeeList: action.data
            });
        break;

        case ABOUT_US.FT:
            return Object.assign({}, state, {
                ft: action.data
            });
        break;

        default:
            return state;
    }
};