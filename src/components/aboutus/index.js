import { Component } from 'react';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import logger from 'redux-logger';

import rootReducer from './reducers';
const store = createStore(rootReducer, compose(
    applyMiddleware(thunk, logger),
    window.devToolsExtension ? window.devToolsExtension() : f => f
));

import AboutUsComponent from './component';

export default class AboutUs extends Component {
    constructor(props) {
        super(props);
    }
    render(){
        return (
            <Provider store={store}>
        	    <AboutUsComponent></AboutUsComponent>
            </Provider>
        )
    }
}