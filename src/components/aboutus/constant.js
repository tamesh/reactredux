const ABOUT_US_CONSTANT = Object.freeze({
    PENDING: "PENDING",
    DONE: "DONE",
    ERROR: "ERROR",
    EMPLOYEE_LIST: "EMPLOYEE_LIST",
    FT: "FT",
    PT: "PT",
    PR: "PR"
});

export default ABOUT_US_CONSTANT;