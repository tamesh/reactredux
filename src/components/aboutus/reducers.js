import { combineReducers } from 'redux';

import aboutUsReducers from './about-us-reducer';

export default combineReducers({ aboutUsReducers })