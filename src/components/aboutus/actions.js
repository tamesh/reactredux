import axios from 'axios';
import ABOUT_US from './constant';

export const getList = () => {
    return dispatch => {
        
        axios.get('http://localhost:3000/employees')
            .then(response => {
                dispatch({
                    type: ABOUT_US.EMPLOYEE_LIST,
                    data: response.data
                });
            })
            .catch( () => dispatch({ type: ABOUT_US.ERROR }) )
    }
};

export const getFT = () => {
    return dispatch => {
        dispatch({ type: ABOUT_US.PENDING });
        
        axios.get('http://localhost:3000/FT')
            .then(response => {
                dispatch({
                    type: ABOUT_US.FT,
                    data: response.data
                });
            })
            .catch( () => dispatch({ type: ABOUT_US.ERROR }) )
    }
};

export const getPT = () => {
    return dispatch => {
        dispatch({ type: ABOUT_US.PENDING });
        
        axios.get('http://localhost:3000/PT')
            .then(response => {
                dispatch({
                    type: ABOUT_US.PT,
                    data: response.data
                });
            })
            .catch( () => dispatch({ type: ABOUT_US.ERROR }) )
    }
};

export const getPR = () => {
    return dispatch => {
        dispatch({ type: ABOUT_US.PENDING });
        
        axios.get('http://localhost:3000/PR')
            .then(response => {
                dispatch({
                    type: ABOUT_US.PR,
                    data: response.data
                });
            })
            .catch( () => dispatch({ type: ABOUT_US.ERROR }) )
    }
};