import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { getList, getFT, getPT, getPR } from './actions';

const mapStateToProps = state => {
    return {
        employeeList: state.aboutUsReducers.employeeList,
        ft: state.aboutUsReducers.ft,
        pt: state.aboutUsReducers.pt,
        pr: state.aboutUsReducers.pr
    }
};

const mapDispatchToProps = dispatch => {
    return {
        actions: {
            getList: () => dispatch(getList()),
            getFT: () => dispatch(getFT()),
            getPT: () => dispatch(getPT()),
            getPR: () => dispatch(getPR())
        }
    }
};

const Heading = (props) => {
    return (
        <li className="list-group-item"> { props.title } </li>
    )
};

const EmployeeList = (props) => {
    const list = props.dataList;
    return (
        <ul className="list-group">
            { 
                list.map((data) =>
                    <Heading title={data.first_name} key={data.id} />
                )
            }
        </ul>
    )
};

const EmployeeDetails = (props) => {
    const list = props.dataList;
    return (
        <ul className="list-group">
            { 
                list.map((data) =>
                    <li className="list-group-item d-flex justify-content-between align-items-center" key={data.id}>

                        <span> { data.frq } </span>
                        <span className="badge badge-primary badge-pill"> { Math.round(data.ta) } </span>
                    </li>
                )
            }
        </ul>
    )
};

class AboutUsComponent extends Component{
    constructor(props){
        super(props);
    }

    componentDidMount(){
        this.props.actions.getList();
        this.props.actions.getFT();
        this.props.actions.getPT();
        this.props.actions.getPR();
	}

    render() {
        console.log( this.props.ft );
        return(
            <div className="container">
                <div className="row">
                    <div className="col-md">
                        <EmployeeList dataList={this.props.employeeList}></EmployeeList>
                    </div>
                    <div className="col-md">
                        <EmployeeDetails dataList={this.props.ft}></EmployeeDetails>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AboutUsComponent);