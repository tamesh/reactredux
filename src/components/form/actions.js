import * as c from './constants';

export const update = (name, value) => {
  return dispatch => dispatch({
    type: c.FORM_UPDATE_VALUE,
    name, value
  });
}

export const reset = () => {
  return dispatch => dispatch({
    type: c.FORM_RESET
  });
}