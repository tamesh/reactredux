import React, { Component } from 'react';
import RaisedButton from 'material-ui/lib/raised-button';
import PropTypes from "prop-types";

export default class SubmitButton extends Component {

    getDefaultProps() {
        return {
            label: 'Submit'
        };
    }

    render() {
        return ( 
            <RaisedButton primary disabled={!this.context.isFormValid()}
                label={ this.props.label }
                onTouchTap={ this.context.submit }
            />
        );
    }
}


SubmitButton.propTypes = {
    label: PropTypes.string
};

SubmitButton.contextTypes = {
    isFormValid: PropTypes.func.isRequired,
    submit: PropTypes.func.isRequired
}