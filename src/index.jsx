import { Component } from 'react';
import { render } from 'react-dom';
import { HashRouter, BrowserRouter as Router, Route, NavLink, Switch } from 'react-router-dom';
import './assets/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';

import Home from './components/home/index';
import AboutUs from './components/aboutus/index';
import Login from './components/login/index';
import ContactUs from './components/contact-us/component';
import Jira from './components/my-jira/index';

render(
        <HashRouter>
            <div className="container-fluid">
                <header className="row">
                    <div className="col">
                        <h1>React Redux</h1>    
                    </div>
                    <div className="col text-right">
                        <section>
                            <span>tnirmalkar</span>
                        </section>
                        <span>
                            <a href="/sign-out">Sign Out</a>
                        </span>
                    </div>
                </header>
                <div className="row">
                    <nav className="col-2">
                        <ul className="list-group">
                            <li className="list-group-item">
                                <NavLink exact replace to="/" activeClassName="text-danger">Dashboard</NavLink>
                            </li>
                            <li className="list-group-item">
                                <NavLink exact replace to="/my-profile" activeClassName="text-danger">Profile</NavLink>
                            </li>
                            <li className="list-group-item">
                                <NavLink exact replace to="/my-jira" activeClassName="text-danger">My Jira Task</NavLink>
                            </li>
                            <li className="list-group-item">
                                <NavLink exact replace to="/login" activeClassName="text-danger">Login</NavLink>
                            </li>
                            <li className="list-group-item">
                                <NavLink exact replace to="/contact-us" activeClassName="text-danger">Contact Us</NavLink>
                            </li>
                        </ul>
                    </nav>
                    <section className="col-10">
                        <Switch>
                            <Route exact path="/" render={(props) => <Home {...props} name={ "Tameshwar" } /> } />
                            <Route path="/my-profile" render={(props) => <AboutUs {...props} pageHeading={"About us"} />} />
                            <Route path="/my-jira" render={(props) => <Jira {...props} pageHeading={"Jira"} />} />
                            <Route path="/login" render={(props) => <Login {...props} />} />
                            <Route path="/contact-us" render={(props) => <ContactUs {...props} />} />
                        </Switch>
                    </section>
                </div>
            </div>
        </HashRouter>,
    	document.querySelector('#root')
);